public interface TransportWizytor {
    public void visit (Animal animal);
    public void visit (Person person);
    public void visit (Shipment shipment);
}
