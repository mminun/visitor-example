public class NazwaPaczkiTransportWizytor implements TransportWizytor {
    public void visit(Animal animal) {
        System.out.println("wioze zwierzaka " + animal.getRodzaj());
    }

    public void visit(Person person) {
        System.out.println("wioze persona " + person.getFristName()
                + " " + person.getLastName());

    }

    public void visit(Shipment shipment) {
        System.out.println("wioze paczke o prefixie i numerze:" +
                " " + shipment.getPrefix() + " " + shipment.getSerialNumber());

    }
}
