public interface Transportable {

    public void accept(TransportWizytor transportWizytor);
}
