public class CenaPaczkiTransportWizytor implements TransportWizytor {
    public void visit(Animal animal) {
        System.out.println("wyliczam cene za kilometr w zaleznosci od tego co bylo " +
                "transportowane");
        System.out.println("przewozisz " + animal.getRodzaj() + " " +
                " a cena za km to " + animal.getWaga() * 0.2);

    }

    public void visit(Person person) {
        System.out.println("wyliczam cene za kilometr w zaleznosci od tego co bylo " +
                "transportowane");
        int price = 6;
        if (person.isRegularCustomer()) {
            price = price / 2;
        }
        System.out.println("przewozisz " + person.getFristName() + " " +
                " a cena za km to " + price);
    }

    public void visit(Shipment shipment) {
        System.out.println("wyliczam cene za kilometr w zaleznosci od tego co bylo " +
                "transportowane");
        int price = 2;
        if (shipment.isLarge()) {
            price = price * 3;
        }
        System.out.println("przewozisz paczke o numerze " + shipment.getSerialNumber() + " " +
                " a cena za km to " + price);
    }
}
