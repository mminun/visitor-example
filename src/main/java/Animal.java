public class Animal implements Transportable {
    private String rodzaj;
    private int waga;

    public Animal(String rodzaj, int waga) {
        this.rodzaj = rodzaj;
        this.waga = waga;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public int getWaga() {
        return waga;
    }

    public void accept(TransportWizytor transportWizytor) {
        transportWizytor.visit(this);
    }
}
