import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("pies", 30);
        Shipment shipment = new Shipment("+48", "59684", false);
        Person person = new Person("maja", "kujawska",
                true);

        List<Transportable> transportables = Arrays.asList(animal, shipment, person);
        NazwaPaczkiTransportWizytor nazwaPaczkiTransportWizytor = new NazwaPaczkiTransportWizytor();
        final CenaPaczkiTransportWizytor cenaPaczkiTransportWizytor = new CenaPaczkiTransportWizytor();

        transportables.forEach(obiekt -> obiekt.accept(cenaPaczkiTransportWizytor));
        System.out.println("----------");

        transportables.forEach(obiekt -> obiekt.accept(nazwaPaczkiTransportWizytor));
    }
}
